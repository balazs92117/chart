# Upgrade compatibility values

Some changes may require overriding values for previous version install for upgrade tests.

Allow to temporary define override values until release is created.
